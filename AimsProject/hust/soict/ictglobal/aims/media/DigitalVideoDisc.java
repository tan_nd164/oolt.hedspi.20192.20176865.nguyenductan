package hust.soict.ictglobal.aims.media;

public class DigitalVideoDisc extends Media{
	
	private String director;
	private int length;
	
	public String getDirector() {
		return director;
	}
	
	public void setDirector(String director) {
		this.director = director;
	}
	
	public int getLength() {
		return length;
	}
	
	public void setLength(int length) {
		this.length = length;
	}
	
	public DigitalVideoDisc() {
	
	}
	
	public DigitalVideoDisc(int id) {
	    super(id);
	}
	
	public DigitalVideoDisc(String title) {
	    super(title);
	  
	}
  
	public DigitalVideoDisc(String title, String category) {
	    super(title, category);
	  
	}
 
	public DigitalVideoDisc(String title, String category, String director, int length) {
	    super(title, category);
	    this.director = director;
	    this.length = length;
	 
	}
}
/*
Question: If you create a constructor method to build a DVD by title then create a constructor method to build a DVD by category. Does JAVA allow you to do this?
Answer: If i create a constructor method to build a DVD by title then create a constructor method to build a DVD by category. Java doesn't allow to do this unless rename the second constructor
*/
