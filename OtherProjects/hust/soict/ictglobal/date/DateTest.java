package hust.soict.ictglobal.date;

public class DateTest {

	public static void main(String[] args) {
		MyDate date1 = new MyDate();
		MyDate date2 = new MyDate(29,10,1999);
		MyDate date3 = new MyDate();
		date3 = date3.accept();
		
		System.out.print("Date 1: ");
		date1.print();
		System.out.print("Date 2: ");
		date2.print();
		System.out.print("Date 3: ");
		date3.print();
	}

}
