package hust.soict.ictglobal.date;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.TimeZone;
import java.util.Calendar;
import java.util.Scanner;

public class MyDate {
	Calendar localCalendar = Calendar.getInstance(TimeZone.getDefault());
	
	private int day, month, year;

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		switch (this.month) {
		case 1:
		case 3:
		case 5:
		case 7:
		case 8:
		case 10:
		case 12:
			if (day >= 1 && day <= 31)
				this.day = day;
			else
				System.out.println("The date is not valid with this day " + day);
			break;
		case 4:
		case 6:
		case 9:
		case 11:
			if (day >=1 && day <= 30)
				this.day = day;
			else
				System.out.println("The date is not valid with this day " + day);
			break;
		case 2:
			if (this.year % 4 == 0)
			{
				if (day >=1 && day <= 29)
					this.day = day;
				else
					System.out.println("The date is not valid with this day " + day);
			}else {
				if (day >=1 && day <= 28)
					this.day = day;
				else
					System.out.println("The date is not valid with this day " + day);
			}
			break;
		default:
			if (day >= 1 && day <= 31)
				this.day = day;
			break;
		}
	}
	public void setDay(String day) {
		day = day.toLowerCase();
		switch (day) {
		case "first": this.setDay(1); break;
		case "second": this.setDay(2); break;
		case "third": this.setDay(3); break;
		case "fourth": this.setDay(4); break;
		case "fifth": this.setDay(5); break;
		case "sixth": this.setDay(6); break;
		case "seventh": this.setDay(7); break;
		case "eighth": this.setDay(8); break;
		case "ninth": this.setDay(9); break;
		case "tenth": this.setDay(10); break;
		case "eleventh": this.setDay(11); break;
		case "twelfth": this.setDay(12); break;
		case "thirteenth": this.setDay(13); break;
		case "fourteenth": this.setDay(14); break;
		case "fifteenth": this.setDay(15); break;
		case "sixteenth": this.setDay(16); break;
		case "seventeenth": this.setDay(17); break;
		case "eighteenth": this.setDay(18); break;
		case "nineteenth": this.setDay(19); break;
		case "twentieth": this.setDay(20); break;
		case "twenty first": this.setDay(21); break;
		case "twenty second": this.setDay(22); break;
		case "twenty third": this.setDay(23); break;
		case "twenty fourth": this.setDay(24); break;
		case "twenty fifth": this.setDay(25); break;
		case "twenty sixth": this.setDay(26); break;
		case "twenty seventh": this.setDay(27); break;
		case "twenty eighth": this.setDay(28); break;
		case "twenty ninth": this.setDay(29); break;
		case "thirtieth": this.setDay(30); break;
		case "thirty first": this.setDay(31); break;
		}
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		switch (month) {
		case 1:
		case 3:
		case 5:
		case 7:
		case 8:
		case 10:
		case 12:
			if (this.day >= 1 && this.day <= 31)
				this.month = month;
			else
				System.out.println("The date is not valid with this month " + month);
			break;
		case 4:
		case 6:
		case 9:
		case 11:
			if (this.day >=1 && this.day <= 30)
				this.month = month;
			else
				System.out.println("The date is not valid with this month " + month);
			break;
		case 2:
			if (this.year % 4 == 0)
			{
				if (this.day >=1 && this.day <= 29)
					this.month = month;
				else
					System.out.println("The date is not valid with this month " + month);
			}else {
				if (this.day >=1 && this.day <= 28)
					this.month = month;
				else
					System.out.println("The date is not valid with this month " + month);
			}
			break;
		}
	}

	public void setMonth(String month) {
		month = month.toLowerCase();
		switch(month) {
		case "january": this.setMonth(1); break;
		case "february": this.setMonth(2); break;
		case "march": this.setMonth(3); break;
		case "april": this.setMonth(4); break;
		case "may": this.setMonth(5); break;
		case "june": this.setMonth(6); break;
		case "july": this.setMonth(7); break;
		case "august": this.setMonth(8); break;
		case "september": this.setMonth(9); break;
		case "october": this.setMonth(10); break;
		case "november": this.setMonth(11); break;
		case "december": this.setMonth(12); break;
		}
	}
	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		if (this.day == 29 && this.month == 2 && this.year % 4 != 0)
			System.out.println("The date is not valid with this year " + year);
		else
			this.year = year;
	}
	
    public void setYear(String year) {
        switch (year.toLowerCase()) {
            case "twenty twenty":
                this.year = 2020;
                break;

            case "twenty nineteen":
                this.year = 2019;
                break;

            case "twenty eighteen":
                this.year = 2018;
                break;

            case "twenty seventeen":
                this.year = 2017;
                break;

            case "twenty sixteen":
                this.year = 2016;
                break;

            case "twenty fifteen":
                this.year = 2015;
                break;

            default:
                break;
        }
    }

	public MyDate() {
		super();
		this.year = this.localCalendar.get(Calendar.YEAR);
		this.month = this.localCalendar.get(Calendar.MONTH);
		this.day = this.localCalendar.get(Calendar.DATE);
	}

	public MyDate(int day, int month, int year) {
		super();
		this.setDay(day);
		this.setMonth(month);
		this.setYear(year);
	}
	public MyDate(String strDate) {
		super();
		String [] dmy = strDate.split("\\s");
		setDay(getDayFromString(dmy[1]));
		setMonth(getMonthFromString(dmy[0]));
        setYear(getYearFroString(dmy[2]));
	}
	public MyDate accept() {
		String date;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Input your date: ");
        date = scanner.nextLine();
        MyDate myDate = new MyDate(date);
        return myDate;
	}
	public void print() {
		System.out.println(this.getDay()+"/" + this.getMonth()+ "/" + this.getYear());
	}
	private int getMonthFromString(String month){
        int mo;
        switch (month){
            case "January":
                mo = 1;
            break;
            case "February":
                mo = 2;
            break;
            case "March":
                mo = 3;
            break;
            case "April":
                mo = 4;
            break;
            case "May":
                mo = 5;
            break;
            case "June":
                mo = 6;
            break;
            case "July":
                mo = 7;
            break;
            case "August":
                mo = 8;
            break;
            case "September":
                mo = 9;
            break;
            case "October":
                mo = 10;
            break;
            case "November":
                mo = 11;
            break;
            case "December":
                mo = 12;
            break;

            default:mo = 0;
        }
        return mo;
    }

    private int getDayFromString(String date){
        String day = date.substring(0,2);
        return Integer.parseInt(day);
    }

    private int getYearFroString(String date){
        int year = Integer.parseInt(date);
        if (year <= 0) return 0;
        else return year;
    }
    
}
