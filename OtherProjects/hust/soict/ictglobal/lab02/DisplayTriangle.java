package hust.soict.ictglobal.lab02;
import java.util.Scanner;
public class DisplayTriangle{
    public static void main(String[] args){
        Scanner height = new Scanner(System.in);
        int i, j;

        System.out.print("Input height of triangle n = ");
        int n = height.nextInt();
        while (n<2)
        {
            System.out.print("Please input n > 1. Try again n = ");
            n = height.nextInt();
        }

        for (i = 1; i <= n; i++)
        {
            for (j = 1; j <= n-i; j++)
                System.out.print(" ");
            for (j = 1; j <=2*i-1; j++)
                System.out.print("*");
            System.out.print("\n");
        }
    }
}